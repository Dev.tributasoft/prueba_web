package com.example.demo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;


/**
 * api Spring Application.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	private Calculator calculator = new Calculator();
	@Test
	public void contextLoads() {
		assertEquals(5, calculator.sum(2, 3));
	}

}
