package com.example.demo;

import org.springframework.stereotype.Service;
/**
 * calculatos 1 Spring Application.
 */

@Service
public class Calculator {
    int sum(int a, int b) {
        return a + b;
    }
}

